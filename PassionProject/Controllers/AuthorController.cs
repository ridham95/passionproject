﻿using PassionProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using System.Net;

namespace PassionProject.Controllers
{
    public class AuthorController : Controller
    {
        private BlogCMSContext Db = new BlogCMSContext();

        // GET: Author
        public ActionResult Index()
        {
            return View(Db.Authors.ToList());
        }

        //GET Author create
        public ActionResult Create()
        {
            return View();
        }
        //post create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "AuthorId,AuthorFName,AuthorLName")] Author author)
        {
            if (ModelState.IsValid)
            {
                Db.Authors.Add(author);
                Db.SaveChanges();
                return RedirectToAction("List");
            }

            return View(author);
        }

        public ActionResult List()
        {

            return View(Db.Authors.ToList());
        }


        //get edit

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Author author = Db.Authors.Find(id);
            if (author == null)
            {
                return HttpNotFound();
            }
            return View(author);
        }

        // POST: edit

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "AuthorId,AuthorFName,AuthorLName")] Author author)
        {
            {
                if (ModelState.IsValid)
                {
                    Db.Entry(author).State = EntityState.Modified;
                    Db.SaveChanges();
                    return RedirectToAction("List");
                }
                return View(author);
            }
        }



        //Delete get

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Author author = Db.Authors.Find(id);
            if (author == null)
            {
                return HttpNotFound();
            }
            return View(author);
        }

        //Delete post

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Author author = Db.Authors.Find(id);
            Db.Authors.Remove(author);
            Db.SaveChanges();
            return RedirectToAction("List");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                Db.Dispose();
            }
            base.Dispose(disposing);


        }


        //Details get
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Author author = Db.Authors.Find(id);
            if (author == null)
            {
                return HttpNotFound();
            }
            return View(author);
        }



    }
}