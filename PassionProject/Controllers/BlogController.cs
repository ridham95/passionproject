﻿using PassionProject.Models;
using PassionProject.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace PassionProject.Controllers
{
    public class BlogController : Controller
    {
        private BlogCMSContext Db = new BlogCMSContext();

        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        public ActionResult List()
        { 
            IEnumerable<Blog> blogs = Db.Blogs.ToList();
            return View(blogs);
        }

        public ActionResult New()
        {
            //Connection to DB needed to grant list of authors
            //ask user to pick which author wrote the blog

            BlogEdit blogeditview = new BlogEdit();
            blogeditview.authors = Db.Authors.ToList();

            return View(blogeditview);
        }

        [HttpPost]
        public ActionResult Create(string BlogTitle_New, string BlogDesc_New, int BlogAuthor_New)
        {
            //Standard query representation   
            string query = "insert into Blogs (BlogTitle, BlogDescription, author_AuthorID) values (@title, @desc, @author)";
            SqlParameter[] myparams = new SqlParameter[3];
            myparams[0] = new SqlParameter("@title", BlogTitle_New);
            myparams[1] = new SqlParameter("@desc", BlogDesc_New);
            myparams[2] = new SqlParameter("@author", BlogAuthor_New);
            //forcing the blog to have an author

            //putting that information into the database by running a command
            Db.Database.ExecuteSqlCommand(query, myparams);

            return RedirectToAction("List");
        }


        public ActionResult Edit(int id)
        {
            //For edit we need a list of authors to pick from
            //we also need to know what the current author is

            BlogEdit blogeditview = new BlogEdit();
            blogeditview.authors = Db.Authors.ToList();
            //Line equivalent to "Select * from blogs where blogid = .."
            blogeditview.blog = Db.Blogs.Find(id);

            return View(blogeditview);
        }

        [HttpPost]
        public ActionResult Edit(int id, string BlogTitle, int BlogAuthor, string BlogDesc)
        {
            //If the ID doesn't exist or the blog doesn't exist
            if ((id == null) || (Db.Blogs.Find(id) == null))
            {
                return HttpNotFound();

            }
            string query = "update Blogs set BlogTitle=@title, BlogDescription=@desc, author_AuthorID=@author where blogid=@id";
            SqlParameter[] myparams = new SqlParameter[4];
            myparams[0] = new SqlParameter("@title", BlogTitle);
            myparams[1] = new SqlParameter("@desc", BlogDesc);
            myparams[2] = new SqlParameter("@author", BlogAuthor);
            myparams[3] = new SqlParameter("@id", id);
            //forcing the blog to have an author

            Db.Database.ExecuteSqlCommand(query, myparams);

            return RedirectToAction("Show/" + id);
        }

        public ActionResult Show(int? id)
        {
    
            if ((id == null) || (Db.Blogs.Find(id) == null))
            {
                return HttpNotFound();

            }
            string query = "select * from blogs where blogid=@id";
 
            SqlParameter[] myparams = new SqlParameter[1];
            myparams[0] = new SqlParameter("@id", id);
            
            Blog myblog = Db.Blogs.SqlQuery(query, myparams).FirstOrDefault();

            return View(myblog);
        }




    }
}