﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PassionProject.Models
{
    public class Blog
    {

        [Key, ScaffoldColumn(false)]
        public int BlogId { get; set; }

        [Required, StringLength(255), Display(Name = "Title")]
        public string BlogTitle { get; set; }

        [StringLength(int.MaxValue), Display(Name = "Description")]
        public string BlogDescription { get; set; }

        //Blog Author
        public virtual Author author { get; set; }

    }
}