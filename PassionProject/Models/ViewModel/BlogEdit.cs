﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PassionProject.Models.ViewModel
{
    public class BlogEdit
    {
        //Empty constructor
        public BlogEdit()
        {

        }

        public virtual Blog blog { get; set; }

        public IEnumerable<Author> authors { get; set; }
    }
}