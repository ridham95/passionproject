﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PassionProject.Models
{
    public class Author
    {

        [Key]
        public int AuthorId { get; set; }

        [Required, StringLength(255), Display(Name = "First Name")]
        public string AuthorFName { get; set; }

        [Required, StringLength(255), Display(Name = "Last Name")]
        public string AuthorLName { get; set; }

        //This is how we can represent a one (author) to many (Blogs) relation
        public virtual ICollection<Blog> Blogs { get; set; }

    }
}